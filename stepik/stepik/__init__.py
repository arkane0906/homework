from stepik.models import Course, Teacher, Student


def main():
	# Создаем новый курс
	# По-умолчанию он Закрыт дял записи
	course = Course('Python Essentials')
	# Создаем учителя
	teacher = Teacher('Linus Toravalds', 'linux4all@mail.ru')
	# Присоединяемся к Курсу
	teacher.join_course(course)
	# Открываем его для Учеников
	teacher.open_course(course)
	# Создаем ученика
	student = Student('Dave Mustaine', 'thrashmetal@forever.com')
	# Присоединяемся к Курсу
	student.join_course(course)
	# Смотрим информацию по Курсу
	print('\nКурс:', course)
	print(course.get_info())
	# Удаляем Учителя из Курса, Курс закроется автоматически
	teacher.leave_course(course)
	# Смотрим информацию по Курсу
	print('\nКурс:', course)
	print(course.get_info())
	# Создаем новый Курс
	new_course = teacher.add_course('Linux Administration')
	# Смотрим информацию по Курсу
	print('\nКурс:', new_course)
	print(new_course.get_info())