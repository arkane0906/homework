from .utils import strict_argument_types
from datetime import datetime


class Course(object):
    """Курс"""

    def __init__(self, title):
        self.title = title  # Название Курса
        self.open = False  # Статус Курса (Закрыт/Открыт)
        self.teachers = []  # Список Учителей
        self.students = []  # Список Учеников

    def add_teacher(self, teacher):
        # Добавляет Учителя к Курсу
        if teacher in self.teachers:
            # Если Учиетль уже есть в списке
            raise ValueError('The Teacher is already on the Course')
        self.teachers.append(teacher)

    def add_student(self, student):
        # Добавляет Ученика к Курсу
        if not self.is_open():
            # Если Курс закрыт
            raise ValueError('The Course is Closed')
        if student in self.students:
            # Если Ученик уже есть в списке
            raise ValueError('The Student is already on the Course')
        self.students.append(student)

    def remove_teacher(self, teacher):
        # Удаляет учителя из Курса
        if teacher not in self.teachers:
            # Если Учителя нет в списке
            raise ValueError('The Teacher is not enrolled in the Course')
        self.teachers.remove(teacher)
        if not len(self.teachers):
            # Если не осталось учителей - закрыть Курс
            self.close_course()

    def remove_student(self, student):
        # Удаляет студента из Курса
        if student not in self.students:
            # Если Студента нет в Списке
            raise ValueError('The Student is not enrolled in the Course')
        self.students.remove(student)

    def get_title(self):
        # Возвращает название Курса
        return self.title

    def get_teachers(self):
        # Возвращает список Учителей
        return self.teachers

    def get_students(self):
        # Возвращает список Учеников
        return self.students

    def open_course(self, teacher):
        # Открывает Курс
        if not len(self.teachers):
            # Если нет Учителей на Курсе - не откроет
            raise ValueError('The Course can\'t be opened without teachers')
        if teacher not in self.teachers:
            # Если пытается октрыть Учитель, который не находится на Курсе
            raise ValueError('The Teacher is not enrolled in the Course')
        self.open = True

    def close_course(self, teacher=None):
        # Закрывает курс
        if teacher and teacher not in self.teachers:
            # Если пытается закрыть Учитель, который не находится на Курсе
            raise ValueError('The Teacher is not enrolled in the Course')
        self.open = False

    def is_open(self):
        # Возвращает boolean, открыт ли Курс?
        return self.open

    def get_info(self):
        # Возвращает всю информацию по Курсу в удобном виде
        return {
            'title': self.get_title(),
            'teachers': self.get_teachers(),
            'students': self.get_students(),
            'is_open': self.is_open(),
        }

    def __repr__(self):
        # Отображает экземпляра Курса как "Название курса"
        return self.title


class User(object):
    """Пользователь"""

    def __init__(self, name, email):
        self.name = name  # Имя
        self.email = email  # E-mail
        self.registered = datetime.now()  # Дата регистрации
        self.courses = []  # Список Курсов

    def get_name(self):
        # Возвращает Имя Пользователя
        return self.name

    def get_email(self):
        # Возвращает E-mail Пользователя
        return self.email

    def get_registered(self):
        # Возвращает дату регистрации Пользователя
        return self.registered

    def get_courses(self):
        # Возвращает Список Курсов Пользователя
        return self.courses

    def get_info(self):
        # Возвращает всю информацию по Пользователю в удобном виде
        return {
            'name': self.get_name(),
            'email': self.get_email(),
            'registered': self.get_registered(),
            'courses': self.get_courses(),
        }

    def __repr__(self):
        # Отображает экземпляра Пользователя как "Имя (E-mail)"
        return '{} ({})'.format(self.name, self.email)


class Teacher(User):
    """Учитель"""

    def __init__(self, name, email):
        super().__init__(name, email)  # Инициализация родительского класса

    def add_course(self, name):
        # Создает Курс и присоединяется к нему
        course = Course(name)
        self.join_course(course)
        return course

    def join_course(self, course):
        # Присоединяется к курсу
        course.add_teacher(self)
        self.courses.append(course)

    def open_course(self, course):
        # Открывает Курс
        if course not in self.courses:
            # Если Учитель не находится на Курсе
            raise ValueError('The Teacher is not enrolled in the Course')
        course.open_course(self)

    def close_course(self, course):
        # Закрывает Курс
        if course not in self.courses:
            # Если Учитель не находится на Курсе
            raise ValueError('The Teacher is not enrolled in the Course')
        course.close_course(self)

    def leave_course(self, course):
        # Покидает Курс
        if course not in self.courses:
            # Если Учитель не находится на Курсе
            raise ValueError('The Teacher is not enrolled in the Course')
        course.remove_teacher(self)
        # Удаляет Курс из Списка Курсов
        self.courses.remove(course)


class Student(User):
    """Ученик"""

    def __init__(self, name, email):
        super().__init__(name, email)  # Инициализация родительского класса

    def join_course(self, course):
        # Присоединяется к курсу
        course.add_student(self)
        self.courses.append(course)

    def leave_course(self, course):
        # Покидает Курс
        if course not in self.courses:
            # Если Ученик не находится на Курсе
            raise ValueError('The Student is not enrolled in the Course')
        course.remove_student(self)
        # Удаляет Курс из Списка Курсов
        self.courses.remove(course)
