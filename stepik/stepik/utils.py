from inspect import signature
from functools import wraps


def strict_argument_types(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        sig = signature(func)
        ba = sig.bind(*args, **kwargs)
        for k, v in ba.arguments.items():
            must_be = sig.parameters[k].annotation
            if must_be != sig.empty and not isinstance(v, must_be):
                raise TypeError(
                    'The argument "{}" must be "{}", passed "{}"'.format(
                        k, must_be, type(v)
                    ))
        return func(*args, **kwargs)
    return wrapper
