# Попробуйте перенести в ОО-код следующий пример из реального мира:
# - есть курсы, учителя и ученики
# - у каждого курса есть свой учитель
# - у каждого учителя есть своя группа учеников
# - у каждого ученика есть свой учитель
# и т.д.

# Определите какие объекты есть в этом примере, какие у них свойства
# и какие методы, как эти объекты будут между собой взаимодействовать,
# например, к курсу можно добавить учителя.

# Создайте все необходимые классы и приведите пример их использования.



from task_07_03 import strict_argument_types


class CourseException(Exception):
    pass


class TeacherException(Exception):
    pass


class StudentException(Exception):
    pass


class Course(object):
	"""Курс"""
	def __init__(self, name, teacher):
		super().__init__()
		self.name = name
		self.teachers = []
		self.add_teacher(teacher)

	@strict_argument_types
    def add_teacher(self, teacher:Teacher):
        self.teachers.append(teacher)

    def get_teachers(self):
    	return self.teachers

    @strict_argument_types
    def remove_teacher(self, teacher:Teacher):
    	return self.teachers


class Teacher(object):
	"""Учитель"""
	def __init__(self, name):
		super().__init__()
		self.name = name
		self.courses = []

	@strict_argument_types
	def add_course(self, course:Course):
		self.courses.append(course)
		course.add_teacher(self)


class Student(object):
	"""Ученик"""
	def __init__(self, name):
		super().__init__()
		self.name = name
		