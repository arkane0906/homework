# Задача. Валидатор входных данных
# По аналогии с примером из лекции про ParamHandler,
# реализовать систему валидации входных данных.

# В системе существует базовый класс Validator

# Интерфейс класса состоит из одного метода validate(value),
# который принимает валидируемое значение. Метод возвращает True,
# если value введено верно, или False в случаи ошибки.

# Очевидно, что класс Validator и метод validate абстрактные.

# Получить требуемый валидатор можно с помощью метода
# get_instance(name), где name - это имя валидатора.

# >>> validator = Validator.get_instance('email')
# >>> validator.validate('info@itmo-it.org')
# True
# >>> validator.validate('break_email')
# False
            
# Если валидатор с именем name не найден, то необходимо
# выбросить исключение ValidatorException с сообщением
# 'Validator with name "{}" not found'.

# >>> validator = Validator.get_instance('unknown')
# Traceback (most recent call last):
#   ...
# ValidatorException: Validator with name "unknown" not found
            
# Чтобы метод get_instance смог работать с именами валидаторов
# и возвращать требуемый экземпляр, необходимо добавить статический
# метод add_type(name, klass), где name - это короткое имя валидатора,
# а klass - ссылка на конкретный валидатор (конкретную реализацию).

# >>> Validator.add_type('email', EMailValidator)
            
# Метод add_type должен выбросить исключение ValidatorException
# с сообщением об ошибке:
# 'Validator must have a name!', если name не указан;
# 'Class "{}" is not Validator!', если klass не наследует от Validator.


# Реализовать два дочерних валидатора:

# EMailValidator - проверяет, что введенный E-Mail корректный.
# Имя валидатора email

# Не торопитесь использовать регулярные выражения,
# тем более вы можете их и не знать.

# Проблема, почему не стоит проверять E-Mail по стандарту
# подробно описана в этой статье

# Еще одна статья на эту тему.

# DateTimeValidator - проверяет, что введенная дата/время корректная. 
# Имя валидатора datetime

# Допустимыми считаются следующие форматы:

# год-месяц-день (2017-09-01, 2017-09-1, 2017-9-1)
# год-месяц-день часы:минуты (2017-09-01 12:00)
# год-месяц-день часы:минуты:секунды (2017-09-01 12:00:00)
# день.месяц.год (1.9.2017, 1.09.2017, 01.09.2017)
# день.месяц.год часы:минуты (1.9.2017 12:00)
# день.месяц.год часы:минуты:секунды (1.9.2017 12:00:00)
# день/месяц/год (1/9/2017, 1/09/2017, 01/09/2017)
# день/месяц/год часы:минуты (1/9/2017 12:00)
# день/месяц/год часы:минуты:секунды (1/9/2017 12:00:00)

# Имена классов
# - Validator
# - EMailValidator
# - DateTimeValidator

# Тестовый набор данных для EMailValidator №1
# Входные данные
# info@itmo-it.org
# Выходные данные
# True

# Тестовый набор данных для EMailValidator №2
# Входные данные
# unknown
# Выходные данные
# False

# Тестовый набор данных для DateTimeValidator №1
# Входные данные
# 1.9.2017
# Выходные данные
# True

# Тестовый набор данных для DateTimeValidator №2
# Входные данные
# 01/09/2017 12:00
# Выходные данные
# True

# Тестовый набор данных для DateTimeValidator №3
# Входные данные
# 2017-09-01 12:00:00
# Выходные данные
# True


from abc import ABCMeta, abstractmethod
from datetime import datetime
from string import digits


class ValidatorException(Exception):
    pass


class Validator(metaclass=ABCMeta):
    validators = {}

    @abstractmethod
    def validate(self):
        pass

    @classmethod
    def add_type(cls, name, validator):
        if not name:
            raise ValidatorException(
                'Validator must have a name!')
        if not issubclass(validator, cls):
            raise ValidatorException(
                'Class "{}" is not Validator!'.format(validator))
        cls.validators[name] = validator

    @classmethod
    def get_instance(cls, name):
        validator = cls.validators.get(name)

        if validator is None:
            raise ValidatorException(
                'Validator with name "{}" not found'.format(name))

        return validator


class EMailValidator(Validator):

    @staticmethod
    def validate(value):
        spt_adress = value.strip().split('@')
        if len(spt_adress) == 2 and len(''.join(spt_adress)) >= 2:
            return True
        return False


class DateTimeValidator(Validator):
    validators = {
        '--': '%Y-%m-%d',
        '--:': '%Y-%m-%d %H:%M',
        '--::': '%Y-%m-%d %H:%M:%S',
        '..': '%d.%m.%Y',
        '..:': '%d.%m.%Y %H:%M',
        '..::': '%d.%m.%Y %H:%M:%S',
        '//': '%d/%m/%Y',
        '//:': '%d/%m/%Y %H:%M',
        '//::': '%d/%m/%Y %H:%M:%S',
    }

    # Старый способ
    #
    # @classmethod
    # def validate(cls, value):
    #     for validator in cls.validators:
    #         try:
    #             datetime.strptime(value, validator)
    #             return True
    #         except ValueError:
    #             continue
    #     return False

    @classmethod
    def validate(cls, value):
        fingerprint = ''.join(
            # Лучше проверять на фхождение символа в [. / - :]
            [x for x in value if x not in list(digits + ' ')]
        )
        validator = cls.validators.get(fingerprint)
        if  not validator:
            return False
        try:
            datetime.strptime(value, validator)
            return True
        except ValueError:
            return False


Validator.add_type('email', EMailValidator)
Validator.add_type('datetime', DateTimeValidator)


def main():
    validator = Validator.get_instance('email')
    print(validator.validate('aaaaa@mail.ru') == True)
    print(validator.validate('afdl.ru') == False)
    print(validator.validate('fdfs@fdfd@afdl.ru') == False)

    validator = Validator.get_instance('datetime')
    print(validator.validate('1.9.2017 12:00') == True)
    print(validator.validate('1/09/2017 12:04:10') == True)
    print(validator.validate('1.34.2017') == False)


if __name__ == '__main__':
    main()
