from random import randint


UNKNOWN = '-'
MISS = '.'
DAMAGED = '!'
DESTROYED = 'X'


class Field(object):

    def __init__(self):
        self.field = [['-' for x in range(10)] for y in range(10)]
        self.ships = []

    def print(self):
        print('   А Б В Г Д Е Ж З И К')
        for index, row in enumerate(self.field, 1):
            print('{0: >2}'.format(index), ' '.join(row))

    def is_valid(self, dots):
        dots_around = set()

        for row, col in dots:
            dots_around.add((row, col))
            dots_around.add((row, col - 1))
            dots_around.add((row, col + 1))
            dots_around.add((row - 1, col))
            dots_around.add((row - 1, col - 1))
            dots_around.add((row - 1, col + 1))
            dots_around.add((row + 1, col))
            dots_around.add((row + 1, col - 1))
            dots_around.add((row + 1, col + 1))

        for row, col in dots_around:
            try:
                # Если не стоит корабль
                if self.field[row][col] == 'X':
                    return False
            except IndexError:
                continue
        return True

    def place_ship(self, size):
        while True:
            decks = []
            row = randint(0, 9)
            col = randint(0, 9)
            direction = ['горизонтальный', 'вертикальный'][randint(0, 1)]
            if direction == 'вертикальный':
                if 9 - row > size:
                    # Вниз
                    if not self.is_valid(
                            ((row + i, col) for i in range(size))):
                        continue
                    for i in range(size):
                        decks.append((row + i, col))
                    break
                else:
                    # Вверх
                    if not self.is_valid(
                            ((row - i, col) for i in range(size))):
                        continue
                    for i in range(size):
                        decks.append((row - i, col))
                    break
            elif direction == 'горизонтальный':
                if 9 - col > size:
                    # Вправо
                    if not self.is_valid(
                            ((row, col + i) for i in range(size))):
                        continue
                    for i in range(size):
                        decks.append((row, col + i))
                    break
                else:
                    # Влево
                    if not self.is_valid(
                            ((row, col - i) for i in range(size))):
                        continue
                    for i in range(size):
                        decks.append((row, col - i))
                    break
        self.ships.append(Ship(decks))

    def get_ships(self):
        return self.ships

    @staticmethod
    def get_coords(colrow):
        chars = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'к']
        nums = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
        if colrow[0].lower() not in chars or colrow[1:] not in nums:
            raise ValueError('Invalid coordinates')
        col = chars.index(colrow[0])
        row = nums.index(colrow[1])
        return row, col

    def strike(self, colrow):
        row, col = self.get_coords(colrow)
        dot = (row, col)
        for ship in self.ships:
            if dot in ship.get_decks():
                status = ship.strike()
                if status == 'Уничтожен':
                    for rw, cl in ship.get_decks():
                        self.field[rw][cl] = DESTROYED
                else:
                    self.field[row][col] = DAMAGED
                return status
        self.field[row][col] = MISS
        return 'Промах'


class Ship(object):

    def __init__(self, decks):
        """decks - список клеток на поле, занимаемые кораблем"""
        self.decks = decks
        self.decks_left = len(decks)

    def strike(self):
        self.decks_left -= 1
        return self.get_status()

    def get_status(self):
        if self.decks_left == len(self.decks):
            return 'Не тронут'
        elif self.decks_left == 0:
            return 'Уничтожен'
        return 'Попадание'

    def get_decks(self):
        return self.decks

    def __repr__(self):
        return 'Ship({})'.format(self.decks)
