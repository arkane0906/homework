from .models import Field, Ship


def main():
    field = Field()
    step = 1

    for i in range(4):
        for j in range(4 - i):
            field.place_ship(j + 1)

    print(field.ships)

    while True:
        field.print()
        print(field.strike(input('Шаг {}: '.format(step))))
        print()
        step += 1
