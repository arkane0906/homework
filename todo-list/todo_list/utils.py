def print_todos(todos):
    # Если todos - один элемент (словарь), все равно добавляем его в список
    if isinstance(todos, dict):
        todos = [todos]
    # Определяем максимальную длину элементов для каждого поля
    # Учитывая сами названия полей

    fields = {
        'id':           'id',
        'status':       'Статус',
        'title':        'Название',
        'description':  'Описание',
        'deadline':     'Дата завершения',
    }
    ss = todos.copy()
    ss.append({x: y for x, y in fields.items()})
    ln = {x: max([len(str(y[x])) for y in ss]) for x in ss[0].keys()}
    header = '| ' + ' | '.join([
        '{:^{width}}'.format(fields['id'], width=ln['id']),
        '{:^{width}}'.format(fields['status'], width=ln['status']),
        '{:^{width}}'.format(fields['title'], width=ln['title']),
        '{:^{width}}'.format(
            fields['description'], width=ln['description']),
        '{:^{width}}'.format(fields['deadline'], width=ln['deadline'])
    ]) + ' |'
    sep = '-' * len(header)
    print(sep)
    print(header)
    print(sep)
    for todo in todos:
        todo_string = '| ' + ' | '.join([
            '{:{width}}'.format(todo['id'], width=ln['id']),
            '{:{width}}'.format(todo['status'], width=ln['status']),
            '{:{width}}'.format(todo['title'], width=ln['title']),
            '{:{width}}'.format(
                todo['description'], width=ln['description']),
            '{:{width}}'.format(todo['deadline'], width=ln['deadline'])
        ]) + ' |'
        if todo['status'] == 'Выполнено':
            todo_string = strike(todo_string)
        print(todo_string)
    print(sep)


def make_todo_dict_from_fetch_result(data):
    """
    Делает словарь с ключами и значениями из кортежей,
    которые приходят в ответах из БД

    НЕАКТУАЛЬНО, ТАК КАК ИСПОЛЬЗУЮ dict_factory()
    """

    fields = ['id', 'title', 'description', 'deadline', 'status']
    if isinstance(data, tuple):
        return dict(zip(fields, data))
    elif isinstance(data, list):
        return [dict(zip(fields, todo)) for todo in data]
    else:
        return None

def strike(text):
    """
    Создает перечеркнутый текст
    """

    result = ''
    for c in text:
        result = result + c + '\u0336'
    return result
