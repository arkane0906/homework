import sqlite3
from datetime import date, timedelta

from todo_list import settings, utils


SQL_SELECT_ALL_TODO = """
    SELECT
        id, title, description, deadline, status
    FROM
        todo
"""

SQL_SELECT_TODO_BY_PK = SQL_SELECT_ALL_TODO + " WHERE id=?"

SQL_SELECT_ALL_TODO_BY_STATUS = SQL_SELECT_ALL_TODO + " WHERE status=?"

SQL_INSERT_TODO = """
    INSERT INTO todo (title, description, deadline) VALUES (?, ?, ?)
"""

SQL_UPDATE_TODO_TITLE = """
    UPDATE todo SET title=? WHERE id=?
"""

SQL_UPDATE_TODO_DESCRIPTION = """
    UPDATE todo SET description=? WHERE id=?
"""

SQL_UPDATE_TODO_STATUS = """
    UPDATE todo SET status=? WHERE id=?
"""

SQL_UPDATE_TODO_DEADLINE = """
    UPDATE todo SET deadline=? WHERE id=?
"""


def dict_factory(cursor, row):
    """Создает словарь из кортежа (строки БД)"""

    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def connect(db_name=None):
    """Подключается к БД"""

    if db_name is None:
        db_name = ':memory:'
    conn = sqlite3.connect(db_name)
    conn.row_factory = dict_factory
    return conn


get_connection = lambda: connect(settings.DB_PATH)


def use_connect_as_conn(func):
    """
    Декоратор, передает подключение к БД функции
    и закрывает его по завершении работы функции

    """

    def wrapper(*args, **kwargs):
        with get_connection() as conn:
            return func(conn, *args, **kwargs)
    return wrapper


@use_connect_as_conn
def initialize(conn, creation_schema):
    """Инициализации структуры БД"""

    with conn, open(creation_schema) as f:
        conn.executescript(f.read())


@use_connect_as_conn
def add_todo(conn, todo):
    """Создает новую задачу"""

    data = (
        todo['title'],
        todo['description'],
        todo['deadline'],
    )

    return conn.execute(SQL_INSERT_TODO, data)


@use_connect_as_conn
def find_all(conn):
    """Возвращает все задачи"""

    return conn.execute(SQL_SELECT_ALL_TODO).fetchall()


@use_connect_as_conn
def find_todo_by_pk(conn, pk):
    """Возвращает TODO по первичному ключу"""

    return conn.execute(SQL_SELECT_TODO_BY_PK, (pk,)).fetchone()


@use_connect_as_conn
def find_todo_by_status(conn, status):
    """Возвращает TODO по статусу"""

    return conn.execute(SQL_SELECT_ALL_TODO_BY_STATUS, (status,)).fetchall()


@use_connect_as_conn
def change_status(conn, pk):
    """
    Автоматически изменяет Статус задачи на противоположный.
    """

    todo = conn.execute(SQL_SELECT_TODO_BY_PK, (pk,)).fetchone()
    if todo['status'] == 'Не выполнено':
        status = 'Выполнено'
    else:
        status = 'Не выполнено'
    return conn.execute(SQL_UPDATE_TODO_STATUS, (status, pk))


@use_connect_as_conn
def change_title(conn, pk, title):
    """Изменяет Заголовок задачи"""

    return conn.execute(SQL_UPDATE_TODO_TITLE, (title, pk))


@use_connect_as_conn
def change_description(conn, pk, description):
    """Изменяет Описание задачи"""

    return conn.execute(SQL_UPDATE_TODO_DESCRIPTION, (description, pk))


@use_connect_as_conn
def change_deadline(conn, pk, deadline):
    """Изменяет Дату задачи"""

    return conn.execute(SQL_UPDATE_TODO_DEADLINE, (deadline, pk))
