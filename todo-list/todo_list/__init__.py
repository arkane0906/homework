import os
import sys
from datetime import datetime, timedelta
from collections import OrderedDict

from todo_list import settings, storage, utils


def display_menu(menu):
    """
    Отображает список доступных действий
    """

    print()
    for option, func in menu.items():
        print(option)


def process(menu, argument=None):
    # actions - список "символ-действие", создается из меню
    actions = {k.strip().split(settings.MENU_SEP)[0]: v for k, v in menu.items()}
    while True:
        display_menu(menu)
        choice = input('Выберите нужный пункт: ')
        action = actions.get(choice)  # Возвращает None если нет команды

        if action:
            if action == 'close':
                return
            action(argument) if argument else action()
        else:
            os.system('clear')
            print('Неизвестная команда')


def select_todo_from_list(status=None):
    os.system('clear')
    if status:
        todos = storage.find_todo_by_status(status)
    else:
        todos = storage.find_all()
    if todos:
        available_ids = [str(x['id']) for x in todos]
        while True:
            utils.print_todos(todos)
            todo_id = input('\nВведите id нужной задачи: ')
            if todo_id in available_ids:
                return todo_id
            else:
                os.system('clear')
                print('Введите id нужной задачи\n')
    else:
        if status:
            print('Нет задач со статусом "{}"'.format(status))
        else:
            print('Нет задач')
        return False


def main_menu_show_all():
    os.system('clear')
    print('Все задачи')
    todos = storage.find_all()
    if todos:
        utils.print_todos(todos)
    else:
        print('Нет задач')


def main_menu_add():
    """
    Создает новую задачу
    """

    todo = {
        'title': get_todo_title(),
        'description': get_todo_description(),
        'deadline': get_todo_deadline()
    }
    os.system('clear')
    if storage.add_todo(todo):
        print('Задача добавлена')
    else:
        print('Ошибка. Задаче не добавлена')


def main_menu_edit():
    todo_id = select_todo_from_list('Не выполнено')
    if todo_id:
        os.system('clear')
        print('Редактирование задачи\n')
        todo = storage.find_todo_by_pk(todo_id)
        utils.print_todos(todo)
        process(edit_todo_menu, argument=todo)
        os.system('clear')


def main_menu_finish():
    todo_id = select_todo_from_list('Не выполнено')
    if todo_id:
	    os.system('clear')
	    if storage.change_status(todo_id):
	        print('Задача помечена как "Выполнено"')
	    else:
	        print('Ошибка. Статус задачи не изменен')


def main_menu_restart():
    # TODO !!! Обновлять дату завершения (просить ввести)
    todo_id = select_todo_from_list('Выполнено')
    if todo_id:
        os.system('clear')
        if storage.change_status(todo_id):
            print('Задача возобновлена')
        else:
            print('Ошибка. Статус задачи не изменен')


def main_menu_exit():
    """
    Выйти из программы
    """

    os.system('clear')
    print('\nДо встречи!\n')
    sys.exit(0)


def edit_menu_edit_title(todo):
    # Редактирование названия
    print('Редактирование названия')
    todo['title'] = get_todo_title()
    os.system('clear')
    if storage.change_title(todo['id'], todo['title']):
        print('Название изменено\n')
    else:
        print('Ошибка. Название не изменено\n')


def edit_menu_edit_description(todo):
    # Редактирование описания
    print('Редактирование описания')
    todo['description'] = get_todo_description()
    os.system('clear')
    if storage.change_description(todo['id'], todo['description']):
        print('Описание изменено\n')
    else:
        print('Ошибка. Описание не изменено\n')


def edit_menu_edit_deadline(todo):
    # Редактирование даты
    print('Редактирование даты')
    todo['deadline'] = get_todo_deadline()
    os.system('clear')
    if storage.change_deadline(todo['id'], todo['deadline']):
        print('Дата изменена\n')
    else:
        print('Ошибка. Дата не изменена\n')


def get_todo_title():
    os.system('clear')
    while True:
        title = input('Название задачи: ')
        if len(title.strip()) > 0:
            return title
        else:
            os.system('clear')
            print('Введите название задачи\n')


def get_todo_description():
    os.system('clear')
    while True:
        description = input('Описание задачи: ')
        if len(description.strip()) > 0:
            return description
        else:
            os.system('clear')
            print('Введите описание задачи\n')


def get_todo_deadline():
    os.system('clear')
    td = datetime.today().date()
    date_links = OrderedDict([
            ('сегодня', td),
            ('завтра', td + timedelta(days=1)),
            ('конец недели', td - timedelta(days=td.weekday()) + timedelta(days=6))
    ])
    while True:
        try:
            print("Работают след. указатели:")
            for link in date_links:
                print(link)
            deadline = input(
                '\nДата завершения в формате {} : '.format(settings.DATE_FORMAT_RUS)
            ).strip().lower()
            link = date_links.get(deadline)
            if link:
                deadline = link
            else:
                deadline = datetime.strptime(deadline, settings.DATE_FORMAT).date()
            if deadline >= datetime.today().date():
                return deadline
            else:
                os.system('clear')
                print('Нельзя создать просроченную задачу.')
                print('Укажите сегодняшний день или будущий.\n')
        except ValueError:
            os.system('clear')
            print('Неверный формат даты')


main_menu = OrderedDict([
    ('1. Показать все задачи', main_menu_show_all),
    ('2. Добавить задачу', main_menu_add),
    ('3. Редактировать задачу', main_menu_edit),
    ('4. Завершить задачу', main_menu_finish),
    ('5. Повторно запустить задачу', main_menu_restart),
    ('\n0. Выход\n', main_menu_exit)
])

edit_todo_menu = OrderedDict([
    ('1. Редактировать Название', edit_menu_edit_title),
    ('2. Редактировать Описание', edit_menu_edit_description),
    ('3. Редактировать Дату', edit_menu_edit_deadline),
    ('\n0. Вернуться в главное меню\n', 'close')
])


def main():
    storage.initialize(settings.SCHEMA_PATH)
    os.system('clear')
    process(main_menu)
