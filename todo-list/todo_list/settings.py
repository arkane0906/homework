import os.path as Path


#====== База Данных ======#

# Абсолютный или относительный путь к файлу БД
DB_PATH = 'db.sqlite'

# Абсолютный путь к файлу со структурой БД
SCHEMA_PATH = Path.join(Path.dirname(__file__), 'schema.sql')


#====== Приложение =======#

# Формат даты
DATE_FORMAT = '%Y.%m.%d'
DATE_FORMAT_RUS = 'ГГГГ.ММ.ДД'

# Разделитель пунктов меню
MENU_SEP = '. '
