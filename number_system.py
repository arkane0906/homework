# Реализуйте модуль number_system для перевода числа из одной 
# системы счисления в другую.
# Модуль должен содержать 6 функций для перевода из десятичной 
# системы счисления в двоичную, восьмеричную, шестнадцатиричную и наоборот:
# - dec2bin(number) - возвращает str
# - dec2oct(number) - возвращает str
# - dec2hex(number) - возвращает str
# - bin2dec(number) - возвращает int
# - oct2dec(number) - возвращает int
# - hex2dec(number) - возвращает int

# (!) Запрещено использовать встроенные функции/методы,
# решающие эту задачу.
# Подсказка. Не спешите писать 6 разных реализаций, подумайте,
# можно ли написать универсальный алгоритм перевода.
# В решении не должно присутствовать операций ввода-вывода.
# Ситуации, когда в исходном числе есть не допустимые цифры (буквы), 
# игнорируются.

# Материал по переводу из одной СС в другую
# http://inf.e-alekseev.ru/text/Schisl_perevod.html

# Тестовый набор данных №1:
# Входные данные: 250
# Выходные данные для функций dec*: "11111010" "372" "FA"
# Тестовый набор данных №2:
# Входные данные: "1010011010"
# Выходные данные: 666
# Тестовый набор данных №3:
# Входные данные: "755"
# Выходные данные: 493
# Тестовый набор данных №4:
# Входные данные: "ABCDEF"
# Выходные данные: 11259375


def dec2any(number, ss):
    """
    Перевод десятичного числа в 2-ную, 8-ную или 16-ную

    number - Число, которое необходимо перевести. Int или Str
    ss - Система счисления. Может быть 2, 8, или 16
    """

    num = int(number)
    result = []
    while num >= ss:
        result.append(str(num % ss))
        num = num // ss
    result.append(str(num % ss))
    hex_map = {
        '10': 'A',
        '11': 'B',
        '12': 'C',
        '13': 'D',
        '14': 'E',
        '15': 'F',
    }
    return(''.join([hex_map[x] if x in hex_map else x for x in result[::-1]]))


def any2dec(number, ss):
    """
    Перевод числа из 2-ной, 8-ной или 16-ной в десятичную

    number - Число, которое необходимо перевести. Int или Str
    ss - Система счисления. Может быть 2, 8, или 16
    """
    hex_map = {
        'a': '10',
        'b': '11',
        'c': '12',
        'd': '13',
        'e': '14',
        'f': '15',
    }
    number = str(number).lower().replace('\'', '').replace('\"', '')
    nums = [int(hex_map[x]) if x in hex_map else int(x) for x in number]
    lenght = len(nums)
    nums_multiply = [nums[x] * ss ** (lenght - 1 - x) for x in range(lenght)]
    result = sum(nums_multiply)
    return(result)


def dec2bin(number):
    return dec2any(number, 2)


def dec2oct(number):
    return dec2any(number, 8)


def dec2hex(number):
    return dec2any(number, 16)


def bin2dec(number):
    return any2dec(number, 2)


def oct2dec(number):
    return any2dec(number, 8)


def hex2dec(number):
    return any2dec(number, 16)
