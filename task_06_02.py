# Требуется реализовать декоратор с параметрами pause,
# который приостанавливает выполнение функции на указанное
# количество секунд.

# В решении пригодится стандартный модуль time.

# Имя функции-декоратора
# - pause

# Пример использования
# @pause(2)
# def func():
#     print('Фунция выполняется с задержкой!')


from functools import wraps
import time


def pause(seconds):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            time.sleep(seconds)
            return func(*args, **kwargs)
        return wrapper
    return decorator
