# Файл "data.txt" заполнен случайными целыми числами,
# числа разделены одним пробелом.
# 1. Сформировать файл "out-1.txt" из элементов файла "data.txt",
# делящихся без остатка на n
# 2. Сформировать файл "out-2.txt" из элементов файла "data.txt",
# возведенных в степень p
# n и p - целые числа, вводимые с клавиатуры.
# Тестовый набор данных №1:
# Содержимое файла data.txt: 48 48 3 75 26 57 53 21 71 15
# Входные данные: 2 3
# Содержимое файла out-1.txt: 48 48 26
# Содержимое файла out-2.txt: 110592 110592 27 421875 17576 185193 148877 9261 357911 3375


n = int(input())  # Делитель
p = int(input())  # Степень

with open('data.txt') as data:
    nums_str = [int(x) for x in data.readline().split()]

    with open('out-1.txt', 'w') as out1:
        answer = list(filter(lambda x: x % n == 0, nums_str))
        out1.write(' '.join([str(x) for x in answer]))

    with open('out-2.txt', 'w') as out2:
        answer = list(map(lambda x: x ** p, nums_str))
        out2.write(' '.join([str(x) for x in answer]))
