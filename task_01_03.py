# Закрепляем условный оператор и операторы сравнения
# Напишите программу, предлагающую пользователю ввести три целых числа.
# Программа выводит их в порядке возрастания, разделяя запятыми.
# Например, если пользователь вводит числа 10 4 6, то программа должна вывести на экран числа 4, 6, 10.
# Если два числа совпадают. то они должны идти одно за другим.
# Например. если пользователь вводит числа 4 5 4, то программа должна вывести на экран 4, 4, 5.
# Запрещено(!) использовать стандартные функции сортировки!
# Входные данные: 10 4 6
# Выходные данные: 4, 6, 10
# Входные данные: 4 5 4
# Выходные данные: 4, 4, 5


lenght = 3
numbers = [input() for x in range(lenght)]
for i in range(lenght - 1):
    for l in range(lenght - 1):
        if float(numbers[l]) > float(numbers[l + 1]):
            numbers[l], numbers[l + 1] = numbers[l + 1], numbers[l]
print(', '.join(numbers))
