# Задача на самостоятельное изучение дополнительного материала
# Python - язык с динамической типизацией.
# Часто это приводит к неочевидным ошибках,
# а так же необходимости добавлять дополнительный
# код с проверкой входных параметров.

# Начиная с версии 3.5, в Python была добавлена возможность
# "подсказывать" типы аргументов функций и тип возвращаемого значения.

# def summa(a:int, b:int) -> int:
#     return a + b
    
# Но этот механизм, как следует из названия, в момент
# выполнения программы не будет выбрасывать исключения,
# если тип данных переданного аргумента отличается от ожидаемого.

# Требуется реализовать два декоратора: strict_argument_types
# и strict_return_type.

# Декоратор strict_argument_types выбрасывает исключение
# TypeError с сообщением об ошибке
# 'The argument "{}" must be "{}", passed "{}"',
# если один из типов данных аргументов функции не
# соответствует ожидаемому типу.

# Декоратор strict_return_type выбрасывает исключение
# TypeError с сообщением об ошибке
# 'The return value must be "{}", not "{}"' если функция
# возвращает значение, отличное от ожидаемого.

# Рефлексия - это способность компьютерной программы изучать
# и модифицировать свою структуру и поведение
# (значения, мета-данные, свойства и функции) во время выполнения.
# Воспользуемся рефлексией для того, чтобы получить имена
# аргументов функции и уточнения типов.

# Для этого в Python есть стандартный модуль inspect

# Из этого модуля нам нужен метод signature

# Нужно самостоятельно по документации изучить какие
# свойства содержит объект, возвращаемый методом signature
# и какие из этих свойств необходимы для решения задачи.

# Для проверки типа данных переменной использовать функцию
# isinstance(переменная, тип), например, isinstance(a, int).

# Имя функций-декораторов:
# - strict_argument_types
# - strict_return_type

# Пример использования №1
# @strict_argument_types
# @strict_return_type
# def summa(a:int, b:int) -> int:
#     return a + b
            
# Пример использования №2
# @strict_argument_types
# @strict_return_type
# def splitext(path:str) -> (str, str):
#     filename, ext = os.path.splitext(path)
#     return filename, ext.strip('.').lower()


from inspect import signature
from functools import wraps


def strict_argument_types(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        sig = signature(func)
        ba = sig.bind(*args, **kwargs)
        for k, v in ba.arguments.items():
            must_be = sig.parameters[k].annotation
            if must_be != sig.empty and not isinstance(v, must_be):
                raise TypeError(
                    'The argument "{}" must be "{}", passed "{}"'.format(
                        k, must_be, type(v)
                    ))
        return func(*args, **kwargs)
    return wrapper


def strict_return_type(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        must_return = signature(func).return_annotation
        result = func(*args, **kwargs)
        if must_be != sig.empty and not isinstance(result, must_return):
            raise TypeError(
                'The return value must be "{}", not "{}"'.format(
                    must_return, type(result)
                ))
        return result
    return wrapper
