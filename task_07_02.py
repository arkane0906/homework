# Реализовать генератор случайных паролей
# указанной длины.

# В пароле можно использовать любые символы
# в верхнем и нижнем регистре.

# Например: password_generator(16), вернет
# случайные пароли длиной 16 символов.

# Пригодится стандартный модуль random

# Имя функции-генератора
# password_generator

# Возвращаемое значение
# Генератор


from random import randint
from string import ascii_letters, digits, punctuation


def password_generator(l):
    while True:
        chars = list(ascii_letters + digits + punctuation)
        password = ''.join(chars[randint(0, len(chars)-1)] for x in range(l))
        yield password
