from os import path
import json


def read_params(filename):
    # Чтение параметров из файла
    ext = path.splitext(filename)[1][1:]
    read_funcs = {
        'json': read_json
    }
    # Получаем необходимую функцию
    read_file = read_funcs.get(ext)
    # Если формат не поддерживается
    if not read_file:
        raise TypeError(
            'Type "{}" is not supported for reading'.format(ext))
    # Если файл не существует
    if not path.exists(filename):
        raise FileNotFoundError(
            'File "{}" not found'.format(filename))
    # Открываем файл
    with open(filename) as f:
        # Читаем файл с помощью подходящей функции
        return read_file(f)


def write_params(filename, params):
    # Запись параметров в файл
    ext = path.splitext(filename)[1][1:]
    write_funcs = {
        'json': write_json
    }
    # Получаем необходимую функцию
    write_file = write_funcs.get(ext)
    # Если формат не поддерживается
    if not write_file:
        raise TypeError(
            'Type "{}" is not supported for writing'.format(ext))
    # Открываем файл
    with open(filename, 'w') as f:
        # Передаем данные в подходящую функцию для записи в файл
        return write_file(f, params)


# Функции для работы с файлами

def read_json(file_desc):
    return json.loads(file_desc.read())


def write_json(file_desc, data):
    json.dump(data, file_desc, indent=4)


def main():
    # Пример использования

    # Сохраняем конфиг в файл
    conf = {
        'language': 'ru',
        'email': 'lol@mail.ru',
        'notifications': False
    }
    write_params('test-func.json', conf)

    # Читаем конфиг из файла
    config = read_params('test-func.json')
    print(config)

    # Данные целы?
    print('Данные целы?:', conf == config)


if __name__ == '__main__':
    main()
