from abc import ABCMeta, abstractmethod
from os import path


class ParamHandlerException(Exception):
    pass


class ParamHandler(metaclass=ABCMeta):
    handlers = {}

    def __init__(self, source):
        self.source = source
        self.params = {}

    @abstractmethod
    def read(self):
        pass

    @abstractmethod
    def write(self):
        pass

    @classmethod
    def add_handler(cls, type, class_handler):
        if not type:
            raise ParamHandlerException(
                'Please enter a valid file type')
        if not issubclass(class_handler, cls):
            raise ParamHandlerException(
                'Class must be a subclass of a ParamHandler')
        cls.handlers[type] = class_handler

    @classmethod
    def get_instance(cls, source):
        _, ext = path.splitext(source)
        ext = ext.rsplit('.')[-1].lower()
        handler = cls.handlers.get(ext)

        if handler is None:
            raise ParamHandlerException(
                'Unsupported file type "{}"'.format(ext))

        return handler(source)


class TxtHandler(ParamHandler):

    def read(self):
        with open(self.source) as f:
            return f.read()

    def write(self, data):
        with open(self.source, 'w') as f:
            f.write(data)


def main():
    ParamHandler.add_handler('txt', TxtHandler)
    conf = ParamHandler.get_instance('test.txt')
    conf.write('All Works Fine!')
    print(conf.read())

if __name__ == '__main__':
    main()
