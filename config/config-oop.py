from os import path
import json


class Config(object):

    def __init__(self, conf_file=None):
        """
        Если при инициализации указать файл - прочитается его 
        содержимое и запишется в объект.
        Если не указать - в объекте создастся пустой словарь
        для добавления настроек.
        """
        # Методы для чтения указаных форматов
        self.read_methods = {
            'json': self.read_json
        }
        # Методы для записи в указанные форматы
        self.write_methods = {
            'json': self.write_json
        }

        self.conf_file = conf_file
        self.conf = dict()
        if conf_file:
            self.load(conf_file)

    def set(self, key, value):
        """Задает значение ключу в конфиге"""
        self.conf[key] = value

    def set_all(self, conf):
        """Заполняет весь конфиг"""
        self.conf = conf

    def get(self, key, default=None):
        """Получает значение ключа в конфиге"""
        return self.conf.get(key, default)

    def get_all(self):
        """Получает значение ключа в конфиге"""
        return self.conf

    def load(self, filename=None):
        """Помещает значения конф.файла в объект конфига"""
        if filename is None and self.conf_file is not None:
            filename = self.conf_file
        elif filename is None and self.conf_file is None:
            raise ValueError(
                '"filename" is undefined. Try load(filename)')
        # Получаем расширение файла (без точки, для удобства)
        ext = path.splitext(filename)[1][1:]
        # Получаем необходимый метод для работы с файлом
        read_file = self.read_methods.get(ext)
        # Если формат не поддерживается
        if not read_file:
            raise TypeError(
                'Type "{}" is not supported for reading'.format(ext))
        # Если файл не существует
        if not path.exists(filename):
            # Если файл не существует
            raise FileNotFoundError(
                'File "{}" not found'.format(filename))
        # Открываем файл
        with open(filename) as f:
            self.conf = read_file(f)

    def dump(self, filename=None):
        """Выгружает значения конфигурации в файл"""
        if filename is None and self.conf_file is not None:
            filename = self.conf_file
        elif filename is None and self.conf_file is None:
            raise ValueError(
                '"filename" is undefined. Try dump(filename)')
        # Получаем расширение файла (без точки, для удобства)
        ext = path.splitext(filename)[1][1:]
        # Получаем необходимый метод для работы с файлом
        write_file = self.write_methods.get(ext)
        # Если формат не поддерживается
        if not write_file:
            raise TypeError(
                'Type "{}" is not supported for writing'.format(ext))
        # Открываем или создаем файл
        with open(filename, 'w') as f:
            write_file(f, self.conf)

    # Методы для работы с файлами

    def read_json(self, file_desc):
        return json.loads(file_desc.read())

    def write_json(self, file_desc, data):
        json.dump(data, file_desc, indent=4)


def main():
    # Примеры использования

    # -----------------------------------------------------------------
    # Задача: Записать готовый конфиг в файл
    # -----------------------------------------------------------------
    
    # Создаем пустой экземпляр конфига
    conf = Config()

    settings = {
        'version': '1.2',
        'language': 'ru',
    }

    # Импортируем конфиг из словаря
    conf.set_all(settings)

    # Сохраняем конфиг в файл
    conf.dump('test.json')


    # -----------------------------------------------------------------
    # Задача: Прочитать конфигурацию из файла. Использовать в программе
    # -----------------------------------------------------------------

    # Создаем экземпляр конфига, указываем файл для загрузки конфига
    conf = Config('test.json')

    # Получаем значение
    version = conf.get('version')
    print('Версия:', version)

    # Изменяем значение
    conf.set('version', '1.3')
    version = conf.get('version')
    print('Версия:', version)

    # Получаем значение по-умолчанию, если не указано
    name = conf.get('name', 'Аноним')
    print('Привет,', name)

    # Получаем весь конфиг
    print('Весь конф:', conf.get_all())


if __name__ == '__main__':
    main()
