# Требуется реализовать три декоратора:
# run_on_linux, run_on_macos, run_on_windows.

# Декоратор должен проверять на какой операционной
# системе запущена программа и выполнять декорируемую
# функцию только в том случаи, если условие выполняется,
# в противном случаи функция обертка возвращает None

# Необходимо самостоятельно найти, как определить имя текущей ОС.

# Имя функций-декораторов:
# - run_on_linux
# - run_on_macos
# - run_on_windows

# Пример использования
# @run_on_linux
# def func():
#     print('Функция выполняется только на Linux!')


import sys
from functools import wraps


def run_on_linux(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if sys.platform.startswith('linux'):
            return func(*args, **kwargs)
    return wrapper


def run_on_macos(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if sys.platform.startswith('darwin'):
            return func(*args, **kwargs)
    return wrapper


def run_on_windows(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if sys.platform.startswith('win'):
            return func(*args, **kwargs)
    return wrapper
