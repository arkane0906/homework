# Напишите функцию get_days_to_new_year, которая возвращает
# количество дней, оставшихся до нового года.
# Датой наступления нового года считается 1 января.
# Функция должна корректно работать при запуске в любом году,
# т. е. грядущий год должен вычисляться программно.
# Для решения задачи понадобится стандартный модуль datetime
# https://docs.python.org/3/library/datetime.html
# Требуется реализовать только функцию, решение не должно 
# осуществлять операций ввода-вывода.
# Имя функции: get_days_to_new_year
# Возвращаемое значение: int


from datetime import date

TODAY_INCLUDED = True  # Учитывать сегодняшний день?


def get_days_to_new_year():
    today = date.today()
    new_year = date(today.year + 1, 1, 1)
    days_left = new_year - today
    return days_left.days if TODAY_INCLUDED else days_left.days - 1
