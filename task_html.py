# HTML-теги
# Целью данного домашнего задания является закрепление таких
# пройденных тем как:

# Генераторы
# Задание свойств при помощи декоратора @property
# Специальные методы объектов
# Шаблоны проектирования

# Теория
# Hyper Text Markup Language (язык разметки гипертекста) – это
# стандартный язык разметки документов во Всемирной паутине.

# Веб-страницы или веб-документы это текстовые файлы, обычно
# имеющие расширение *.html, *htm. Для просмотра веб-документов
# используется браузер. Основной единицей разметки в веб-документе
# является тег.

# Тег (tag) – элемент HTML, представляющий из себя текст,
# заключенный в угловые скобки <>. При просмотре html-документа
# в браузере, тэги не видны.

# <тег>
# <тег>...</тег>
    
# Атрибуты тега – содержат дополнительную информацию о теге,
# разделяются между собой пробелом. Атрибут имеет имя и может
# иметь значение. Значение атрибута указывается в двойных кавычках.

# <тег атрибут1="значение" атрибут2="значение">
# <тег атрибут1="значение" атрибут2="значение">...</тег>
    
# Теги бывают двух типов – одиночные и парные (контейнеры):

# одиночный тег используется самостоятельно;
# парный может включать внутри себя другие теги или текст.
# Парные теги состоят из двух частей:

# Открывающий тег – обозначается как и одиночный – <тег>
# Закрывающем тег – используется слэш – </тег>.

# Закрывающий тег не содержит атрибуты!

# Допускается вкладывать в контейнер другие теги, однако
# следует соблюдать порядок - первым закрывается последний открытый тег.

# <div>
#     <p>
#         Lorem <strong>ipsum dolor</strong> sit amet
#     </p>
# </div>
    
# Дочерние теги (или дети) - элементы, которые лежат
# непосредственно внутри данного тега.

# Потомки - все элементы, которые лежат внутри данного тега,
# вместе с их детьми, детьми их детей и так далее.

# Каждый тег имеет пять ссылок на другие теги и один список с тегами:

# parent - ссылка на родительский тег
# previousSibling - ссылка на предыдущий соседний тег
# nextSibling - ссылка на следующий соседний тег
# firstChild - ссылка на первый дочерний тег
# lastChild - ссылка на последний дочерний тег
# children - список всех дочерних тегов
# Рассмотрим их на примере тега <i> из следующего кода:

# <div>
#     <p>
#         <b>Lorem</b> <i>ipsum</i> <s>dolor</s> sit amet
#     </p>
# </div>
    
# parent - тег p
# previousSibling - тег b
# nextSibling - тег s
# firstChild - отсутствует
# lastChild - отсутствует
# children - отсутствуют
# Рассмотрим на примере тега <p>:

# parent - тег div
# previousSibling - отсутствует
# nextSibling - отсутствует
# firstChild - b
# lastChild - s
# children - b, i, s

# Имена классов
# - Tag
# - ContainerTag



# Задача №1. Класс Tag
# Необходимо создать класс Tag. Класс описывает одиночный тег.
# Конструктор класса принимает два аргумента:

# имя тега - обязательный аргумент, строковой тип данных
# атрибуты тега - не обязательный аргумент, тип данных словарь

# 1.1. Свойства класса Tag
# При помощи декоратора @property создайте 6 свойств:

# parent - ссылка на родительский тег
# previous_sibling - ссылка на предыдущий соседний тег
# next_sibling - ссылка на следующий соседний тег
# first_child - ссылка на первый дочерний тег
# last_child - ссылка на последний дочерний тег
# children - генератор, возвращает ссылки на дочерние теги
# Свойства parent, previous_sibling и next_sibling доступны
# для чтения, записи и удаления. Вам необходимо для каждого
# свойства написать getter, setter и deleter.

# Свойства first_child, last_child и children должны выбрасывать
# исключение TagException с сообщением 'Not a container tag!'.

# 1.2. Работа с атрибутами класса Tag
# Атрибуты тега задаются "налету" как обычные свойства объекта.

# >>> a = Tag('a')
# >>> a.href = '/python-developer'
# >>> a.class = 'active'

# >>> img = Tag('img')
# >>> img.src = '/python-developer.svg'
# >>> img.alt = 'Python Разработчик'
    
# Python по-умолчанию позволяет добавлять к объекту любые
# свойства налету. Такое поведение нас не устраивает, т.к. в
# будущем не понятно, какое свойство объекта является атрибутом
# тега. Чтобы запретить этот механизм, нужно в классе описать
# свойство __slots__, которое содержит кортеж атрибутов текущего
# объекта. Теперь при попытке присвоить не существующее свойство
# будет возбуждаться исключение.

# Чтобы все не существующие свойства автоматически становились
# атрибутами тега, вам необходимо переопределить специальные
# методы __getattribute__, __setattr__ и __delattr__.
# Эти методы вызываются неявно при получении значения свойства,
# установке нового значения и удаления свойства.

# Обращаю ваше внимание, что предварительно нужно попробовать
# обратиться к родительской реализации и только в случаи появления
# исключения AttributeError, считать данное свойство атрибутом тега.

# Все перечисленные методы вызываются для любого свойства вне
# зависимости от того, существует оно в объекте или нет.

# При других вариантах реализации, вы попадете в бесконечную рекурсию.

# Готовый пример одного из методов:

# >>> def __getattribute__(self, attr):
# >>> try:
# >>>     return super().__getattribute__(attr)
# >>> except AttributeError:
# >>>     return self.__attributes.get(attr)
    
# 1.3. Конвертация в строку
# В классе необходимо переопределить специальный метод __str__,
# который превращает объект в строку HTML-кода

# >>> img = Tag('img')
# >>> img.src = '/python-developer.svg'
# >>> img.alt = 'Python Разработчик'
# <img src="/python-developer.svg" alt="Python Разработчик">


import weakref


class TagException(Exception):
    pass


class Tag(object):
    """Одиночный тег"""

    __slots__ = (
        '_name', '__attributes', '__parent', '__weakref__',
        '__previous_sibling', '__next_sibling'
    )

    def __init__(self, name, attributes=None):
        if attributes and not isinstance(attributes, dict):
            raise TagException('Attributes must be a dict type!')
        self._name = name
        self.__attributes = attributes if attributes else {}
        self.__parent = None
        self.__previous_sibling = None
        self.__next_sibling = None

    def __getattribute__(self, attr):
        try:
            return super().__getattribute__(attr)
        except AttributeError:
            return self.__attributes.get(attr)

    def __setattr__(self, key, value):
        try:
            return super().__setattr__(key, value)
        except AttributeError:
            self.__attributes[key] = value

    def __delattr__(self, item):
        try:
            return super().__delattr__(item)
        except AttributeError:
            del self.__attributes[item]

    def _attrs_to_str(self):
        return ''.join(
            [' {}="{}"'.format(k, v)
                for k, v in self.__attributes.items() if k and v]
        )

    def __str__(self):
        return '<{}{}>'.format(self._name, self._attrs_to_str())

    @property
    def parent(self):
        return self.__parent() if self.__parent else None

    @parent.setter
    def parent(self, value):
        if not isinstance(value, ContainerTag):
            raise TagException('Parent must be a container tag!')
        # if value is self.__previous_sibling:
        #     self.__previous_sibling = None
        # if value is self.__next_sibling:
        #     self.__next_sibling = None
        self.__parent = weakref.ref(value)

    @parent.deleter
    def parent(self):
        self.__parent = None

    @property
    def previous_sibling(self):
        return self.__previous_sibling() if self.__previous_sibling else None

    @previous_sibling.setter
    def previous_sibling(self, value):
        # Проверяем, является ли тегом
        if not isinstance(value, (Tag, ContainerTag)):
            raise TagException('Previous sibling must be a tag!')
        # # Если тег уже является родительским
        # if value is self.__parent:
        #     self.__parent = None
        # # Если тег уже является следующим соседом
        # if value is self.__next_sibling:
        #     self.__next_sibling = None
        # # Назначаем
        # self.__previous_sibling = value
        self.__previous_sibling = weakref.ref(value)
        # # Если у тега след сосед не экз класса - назначаем
        # if value.next_sibling is not self:
        #     value.next_sibling = self

    @previous_sibling.deleter
    def previous_sibling(self):
        self.__previous_sibling = None

    @property
    def next_sibling(self):
        return self.__next_sibling() if self.__next_sibling else None

    @next_sibling.setter
    def next_sibling(self, value):
        # Проверяем, является ли тегом
        if not isinstance(value, (Tag, ContainerTag)):
            raise TagException('Next sibling must be a tag!')
        # # Если тег уже является родительским
        # if value is self.__parent:
        #     self.__parent = None
        # # Если тег уже является предыдущим соседом
        # if value is self.__previous_sibling:
        #     self.__previous_sibling = None
        # # Назначаем
        # self.__next_sibling = value
        self.__next_sibling = weakref.ref(value)
        # # Если у тега пред сосед не экз класса - назначаем
        # if value.previous_sibling is not self:
        #     value.previous_sibling = self

    @next_sibling.deleter
    def next_sibling(self):
        self.__next_sibling = None

    @property
    def first_child(self):
        raise TagException('Not a container tag!')

    @property
    def last_child(self):
        raise TagException('Not a container tag!')

    @property
    def children(self):
        raise TagException('Not a container tag!')


# Задача №2. Класс ContainerTag
# Необходимо создать новый класс ContainerTag, полученный в
# результате наследования от базового класса Tag.

# Класс описывает парный или контейнерный тег.

# При наследовании свойство __slots__ "теряется", поэтому в
# дочернем классе необходимо его снова описать.
# Если в дочернем классе не планируется добавлять новые свойства,
# то __slots__ задается как пустой кортеж. Если в дочернем классе
# будут добавлены новые свойства, то их необходимо перечислить
# в __slots__. В обоих случаях Python автоматически добавит
# в __slots__ свойства, перечисленные в базовом классе.

# 2.1. Реализация свойств first_child, last_child и children
# В классе-родителе описаны 6 свойств, три из которых выбрасывают
# исключение: first_child, last_child и children.
# Необходимо реализовать данные свойства, напоминаю что они
# должны возвращать:

# first_child - ссылка на первый дочерний тег
# last_child - ссылка на последний дочерний тег
# children - генератор, возвращает ссылки на дочерние теги

# 2.2. Вставка дочерних тегов в контейнер
# В интерфейсе класса должны присутствовать следующие методы:

# append_child(tag) - добавить тег в контейнер после существующих
# тегов (в конец)
# insert_before(tag, next_sibling) - добавить тег в контейнер перед
# указанным next_sibling

# 2.3. Конвертация в строку
# В классе необходимо переопределить специальный метод __str__,
# который превращает объект в строку HTML-кода

# >>> img_1 = Tag('img')
# >>> img_1.src = '/python-developer.svg'
# >>> img_1.alt = 'Python Разработчик'
# >>>
# >>> img_2 = Tag('img')
# >>> img_2.src = '/php-developer.svg'
# >>> img_2.alt = 'PHP Разработчик'
# >>>
# >>> img_3 = Tag('img')
# >>> img_3.src = '/java-developer.svg'
# >>> img_3.alt = 'Java Разработчик'
# >>>
# >>> div = ContainerTag('div')
# >>> div.append_child(img_1)
# >>> div.append_child(img_2)
# >>> div.insert_before(img_3, img_1)

# <div><img src="/java-developer.svg" alt="Java Разработчик"><img src="/python-developer.svg" alt="Python Разработчик"><img src="/php-developer.svg" alt="PHP Разработчик"></div>


class ContainerTag(Tag):
    """Парный тег"""

    __slots__ = ('__children', '__first_child', '__last_child')

    def __init__(self, name, attributes=None):
        super().__init__(name, attributes)
        self.__first_child = None
        self.__last_child = None

    def append_child(self, value):
        # Проверяем, является ли тегом
        if not isinstance(value, (Tag, ContainerTag)):
            raise TagException('Child element must be a tag!')
        # Проверяем, не себя ли мы добавляем как child
        if value is self:
            raise TagException('Invalid action!')
        value.parent = self
        if self.first_child is None:
            self.__first_child = weakref.ref(value)
        elif self.last_child:
            value.previous_sibling = self.last_child
            self.last_child.next_sibling = value
        self.__last_child = weakref.ref(value)

    def insert_before(self, value, next_sibling):
        # Проверяем, является ли тегом
        if not isinstance(value, (Tag, ContainerTag)):
            raise TagException('Child element must be a tag!')
        # Проверяем, не себя ли мы добавляем как child
        if value is self:
            raise TagException('Invalid action!')
        if next_sibling.parent is not self:
            raise TagException(
                "{} not a child of {}".format(next_sibling, self)
            )
        # Назначаем тегу родителя
        value.parent = self
        # Если next_sibling не первый элемент
        if next_sibling.previous_sibling:
            # Назначаем предыдущего соседа
            value.previous_sibling = next_sibling.previous_sibling
            # Меняем следующего соседа у предыдущего соседа
            next_sibling.previous_sibling.next_sibling = value
        else:
            # делаем элемент первым
            self.__first_child = weakref.ref(value)
        # Назначаем следующего соседа
        value.next_sibling = next_sibling
        # Меняем предыдущего соседа у следующего соседа
        next_sibling.previous_sibling = value

    @property
    def first_child(self):
        return self.__first_child() if self.__first_child else None

    @property
    def last_child(self):
        return self.__last_child() if self.__last_child else None

    @property
    def children(self):
        child = self.first_child
        while child:
            yield child
            child = child.next_sibling

    def __str__(self):
        children = ''.join([str(x) for x in self.children])
        return '<{}{}>{}</{}>'.format(
            self._name, self._attrs_to_str(), children, self._name
        )


def main():

    # -------------- ЧАСТЬ 1 -----------------

    # Создаем тег с атрибутами
    img = Tag('img')
    img.src = '/logo.png'
    img.alt = 'Логотип'

    # Тег без аттрибутов
    br = Tag('br')

    # Проверяем __str__ метод
    print(img, br)

    # Указывает один тег в качестве след соседа
    img.next_sibling = br

    # Проверяем соседство тегов
    print('img prev:', img.previous_sibling, '| img next:', img.next_sibling)

    # Проверяем соседство тегов
    print('br prev:', br.previous_sibling, '| br next:', br.next_sibling)

    del br  # При удалении связанного объекта
    print('img prev:', img.previous_sibling, '| img next:', img.next_sibling)

    # -------------- ЧАСТЬ 2 -----------------

    # Создаем паур одиночных тегов
    img = Tag(
        'img',
        attributes={'alt': 'logo', 'src': 'https://idelbaev.ru/logo.png'}
    )
    css = Tag(
        'link',
        attributes={'href': '/css/style.css', 'rel': 'stylesheet'}
    )
    script = Tag(
        'script',
        attributes={'src': '/js/script.js'}
    )

    # Создаем контейнерный тег
    div = ContainerTag(
        'div',
        attributes={'class': 'container'}
    )

    # Наполняем контейнерный тег одиночными
    div.append_child(css)
    div.append_child(img)
    div.insert_before(script, img)

    # Еще один тег
    br = Tag('br')

    # Вставляем в начало
    div.insert_before(br, css)

    # Выводим тег на экран
    print(div)

    # Выводим статус вложенных тегов
    print('Отношения:')
    print('Тег css. Родитель: {}, Слева: {}, Справа: {}'.format(
        repr(css.parent), css.previous_sibling, css.next_sibling
    ))
    print('Тег script. Родитель: {}, Слева: {}, Справа: {}'.format(
        repr(script.parent), script.previous_sibling, script.next_sibling
    ))
    print('Тег img. Родитель: {}, Слева: {}, Справа: {}'.format(
        repr(img.parent), img.previous_sibling, img.next_sibling
    ))


if __name__ == '__main__':
    main()
