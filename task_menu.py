# Задача. Меню приложения
# Вернемся к первому домашнему заданию по ежедневнику.
# Вам необходимо было создать меню и обработчики действий.

# Теперь вам необходимо решить эту задачу используя ОО подход.

# Обработчик в ОО подходе будет называться "Команда".
# Каждую "Команду" можно выполнить. Абстрактный класс для всех
# команд, это Command.

# Интерфейс класса состоит из одного абстрактного метода execute(),
# который запускает выполнение команды.

# Команда может принимать аргументы. Чтобы не нарушать интерфейс,
# эти аргументы лучше передать в конструкторе.
# Количество аргументов и их тип заранее не известен.

# Конкретную команду мы получаем в результате наследования. Например:

# >>> class ShowCommand(Command):
# >>>    def __init__(self, task_id):
# >>>        pass

# >>> class ListCommand(Command):
# >>>    def __init__(self):
# >>>        pass
            
# "Команды" объединяются в "Меню" (класс Menu), где у каждой
# команды есть уникальное имя.

# Интерфейс класса Menu следующий:

# add_command(name, klass) - добавить команду в меню,
# name - имя команды, klass - ссылка на класс команды.

# Метод add_command должен выбросить исключение CommandException
# с сообщением об ошибке:

# 'Command must have a name!', если name не указан;
# 'Class "{}" is not Command!', если klass не наследует от Command.
# >>> menu = Menu()
# >>> menu.add_command('show', ShowCommand)
# >>> menu.add_command('list', ListCommand)
                    
# execute(name, *args, **kwargs) - выполняет команду,
# дополнительно можно указать аргументы, необходимые команде.

# Если в меню не найдена команда, то метод должен выбросить
# исключение CommandException с сообщением об ошибке
# 'Command with name "{}" not found'.

# >>> menu.execute('show', 1)
# >>> menu.execute('list')
# >>> menu.execute('unknown')
# Traceback (most recent call last):
#   ...
# CommandException: Command with name "unknown" not found
                    
# Меню можно проитерировать в цикле for..in.
# В классе Menu необходимо реализовать итератор.

# Для этого в классе Menu необходимо описать два метода:

# __next__() - возвращает следующее значение в итераторе,
# если достигнут конец, то возбуждает исключение StopIteration
# __iter__() - возвращает ссылку на итератор (класс,
# где объявлен метод __next__())
# Обращаю ваше внимание на то, что на каждой итерации возвращается
# кортеж из двух значений: название команды и ссылка на класс.

# Дополнительный материал есть в ссылках ниже.
# Необходимо самостоятельно изучить данную тему!

# Метод __next__() (http://pythonz.net/references/named/object.__next__/)
# Метод __iter__() (http://pythonz.net/references/named/object.__iter__/)
# Пример №1 (http://wiki.bioinformaticsinstitute.ru/wiki/%D0%98%D1%82%D0%B5%D1%80%D0%B0%D1%82%D0%BE%D1%80%D1%8B_%D0%B2_Python#.D0.9F.D1.80.D0.BE.D1.82.D0.BE.D0.BA.D0.BE.D0.BB_.D0.B8.D1.82.D0.B5.D1.80.D0.B0.D1.86.D0.B8.D0.B8)
# Пример №2 (Создание собственных итераторов) (http://devpractice.ru/python-lesson-15-iterators-and-generators/)

# >>> for item in menu:
# >>>    print(item)
# ('show', <class '__main__.ShowCommand'>)
# ('list', <class '__main__.ListCommand'>)

# >>> for name, command in menu:
# >>>    print(name, command)
# show <class '__main__.ShowCommand'>
# list <class '__main__.ListCommand'>
           
# Имена классов
# - Command
# - Menu


from abc import ABCMeta, abstractmethod
from collections import OrderedDict


class CommandException(Exception):
    pass


class Command(metaclass=ABCMeta):

    @abstractmethod
    def execute(self):
        pass


class Menu(object):

    def __init__(self):
        self.__commands = OrderedDict()
        self.__index = 0

    def __iter__(self):
        self.__index = len(self.__commands)
        return self

    def __next__(self):
        if self.__index == 0:
            raise StopIteration
        idx = len(self.__commands) - self.__index
        key = list(self.__commands.keys())[idx]
        self.__index = self.__index - 1
        return key, self.__commands[key]

    def add_command(self, name, command):
        if not name:
            raise CommandException(
                'Command must have a name!')
        if not issubclass(command, Command):
            raise CommandException(
                'Class "{}" is not Command!'.format(command))
        self.__commands[name] = command

    def execute(self, name, *args, **kwargs):
        if name not in self.__commands:
            raise CommandException(
                'Command with name "{}" not found'.format(name))

        self.__commands[name](*args, **kwargs).execute()


class ShowCommand(Command):

    def __init__(self, task_id):
        self.__task_id = task_id

    def execute(self):
        print('show task {}'.format(self.__task_id))


class ListCommand(Command):

    def __init__(self):
        pass

    def execute(self):
        print('list')


def main():

    menu = Menu()

    menu.add_command('show', ShowCommand)
    menu.add_command('list', ListCommand)

    menu.execute('show', 1)
    menu.execute('list')

    for cmd in menu:
        print(cmd)

    for name, command in menu:
        print(name, command)


if __name__ == '__main__':
    main()
